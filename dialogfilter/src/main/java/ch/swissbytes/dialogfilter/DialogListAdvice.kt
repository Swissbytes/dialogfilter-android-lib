package ch.swissbytes.dialogfilter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView

class DialogListAdvice(private val windowContext: Context) {

    private val TAG = this::class.java.simpleName

    private var selection: Pair<Int, String>? = null
    fun show(
        items: List<String>,
        @StringRes titleRes: Int? = null,
        @StringRes adviceRes: Int = R.string.fake_advice,
        @StringRes negativeRes: Int = android.R.string.cancel,
        @StringRes positiveRes: Int = android.R.string.ok,
        showDivider: Boolean = false,
        listener: (Pair<Int, String>) -> Unit
    ){
        val dialog = MaterialDialog(windowContext).show {
            customView(R.layout.advice_dialog, noVerticalPadding = false)
            titleRes?.let { title(it) }
            negativeButton(negativeRes)
            positiveButton(positiveRes) {
                selection?.let { listener.invoke(it) }
                dismiss()
            }
        }

        dialog.setActionButtonEnabled(WhichButton.POSITIVE, false)

        with(dialog.getCustomView()){
            val rv = findViewById<RecyclerView>(R.id.rv)
            findViewById<View>(R.id.line_top).visibility = if (showDivider) View.VISIBLE else View.GONE
            findViewById<View>(R.id.line_bottom).visibility = if (showDivider) View.VISIBLE else View.GONE
            val advice = findViewById<TextView>(R.id.advice)
            advice.setText(adviceRes)
            with(rv){
                adapter = Adapter(items.toMutableList()) {
                    dialog.setActionButtonEnabled(WhichButton.POSITIVE, true)
                    selection = it
                }
            }
        }
    }

    class Adapter(val items: MutableList<String>, val listener: (Pair<Int, String>) -> Unit): RecyclerView.Adapter<Adapter.ViewHolder>() {

        private var currentItemsChecked: RadioButton? = null

        override fun getItemCount(): Int = items.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.md_listitem_singlechoice,
                    parent,
                    false
                )
            )

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            with(holder){
                tv.text = item
                itemView.setOnClickListener {
                    currentItemsChecked?.isChecked = false
                    rd.isChecked = true
                    currentItemsChecked = rd
                    listener.invoke(Pair(position, item))
                }
            }
        }

        class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
            val tv = view.findViewById<TextView>(R.id.md_title)
            val rd = view.findViewById<RadioButton>(R.id.md_control)
        }

    }

}

