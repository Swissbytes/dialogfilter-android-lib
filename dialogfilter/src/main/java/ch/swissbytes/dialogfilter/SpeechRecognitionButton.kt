package ch.swissbytes.dialogfilter

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ImageButton
import androidx.core.content.ContextCompat
import ch.swissbytes.dialogfilter.SpeechRecognitionHelper.SpeechRecognitionHelper
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener


class SpeechRecognitionButton : ImageButton {

    private val TAG = this::class.java.simpleName
    private var numberOnly: Boolean = false
    private var speechRecognition: SpeechRecognitionHelper? = null

    var onSpeechStart: SimpleListener? = null
    var onSpeechStop: SimpleListener? = null
    var onError: GenericListener<Int>? = null
    var onResult: GenericListener<String>? = null

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.SpeechRecognitionButton, defStyle, 0)
        numberOnly = a.getBoolean(R.styleable.SpeechRecognitionButton_numbersOnly, false)
        if (android.os.Build.VERSION.SDK_INT > 21){
            backgroundTintList = ContextCompat.getColorStateList(context, android.R.color.transparent)
        }
        setImageResource(R.drawable.ic_keyboard_voice_black_24dp)
        a.recycle()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        speechRecognition = SpeechRecognitionHelper(
            context,
            numberOnly = numberOnly,
            onSpeechStart = onSpeechStart,
            onSpeechStop = onSpeechStop,
            onError = onError,
            onResult = onResult
        )

        setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                speechRecognition?.startRecognition()
            } else if (event.action == MotionEvent.ACTION_UP) {
                speechRecognition?.stopRecognition()
            }
            false
        }
    }


    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        speechRecognition?.onDestroy()
    }

}
