package ch.swissbytes.dialogfilter.SpeechRecognitionHelper

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import ch.swissbytes.dialogfilter.R
import ch.swissbytes.syscomappbase.extensions.isNumber
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener

class SpeechRecognitionHelper(
    context: Context,
    private val numberOnly: Boolean = false,
    private var onSpeechStart: SimpleListener? = null,
    var onSpeechStop: SimpleListener? = null,
    var onError: GenericListener<Int>? = null,
    var onResult: GenericListener<String>? = null
) {

    private val TAG = this::class.java.simpleName
    private var sr: SpeechRecognizer? = null

    private val recognizerIntent by lazy {
        Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "voice.recognition.test")
            putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-ES");
            putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
        }
    }

    private var recognitionListener = object: RecognitionListener {
        override fun onReadyForSpeech(params: Bundle) {
            Log.d(TAG, "onReadyForSpeech")
            onSpeechStart?.invoke()
        }

        override fun onBeginningOfSpeech() {
            Log.d(TAG, "onBeginningOfSpeech")
        }

        override fun onRmsChanged(rmsdB: Float) {
            Log.d(TAG, "onRmsChanged")
        }

        override fun onBufferReceived(buffer: ByteArray) {
            Log.d(TAG, "onBufferReceived")
        }

        override fun onEndOfSpeech() {
            Log.d(TAG, "onEndofSpeech")
            onSpeechStop?.invoke()
        }

        override fun onError(error: Int) {
            Log.d(TAG, "error $error")
            val errorRes = when (error){
                SpeechRecognizer.ERROR_AUDIO -> R.string. voice_recognition_error_audio
                SpeechRecognizer.ERROR_NO_MATCH -> R.string.voice_recognition_error_not_match
                SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> R.string.voice_recognition_error_busy
                SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS ->  R.string.voice_recognition_error_permission
                else -> R.string.voice_recognition_error_unknown
            }
            onError?.invoke(errorRes)
        }

        override fun onResults(results: Bundle) {
            Log.d(TAG, "voiceRecognition: numberOnly: ${numberOnly} onResults $results")
            val data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            if (data != null && data.size > 0){
                val result = data[0].replace(" ", "").trim()
                if (numberOnly){
                    if (result.isNumber()) {
                        onResult?.invoke(result)
                    }
                } else {
                    onResult?.invoke(result)
                }

            }
        }

        override fun onPartialResults(partialResults: Bundle) {
            Log.d(TAG, "onPartialResults")
        }

        override fun onEvent(eventType: Int, params: Bundle) {
            Log.d(TAG, "onEvent $eventType")
        }
    }

    init {
//        requestRecordAudioPermission()
        sr = SpeechRecognizer.createSpeechRecognizer(context)
        sr!!.setRecognitionListener(recognitionListener)
    }

//    private fun requestRecordAudioPermission() {
//        val requiredPermission = Manifest.permission.RECORD_AUDIO
//
//        // If the user previously denied this permission then showItemsCount a message explaining why
//        // this permission is needed
//        if (checkCallingOrSelfPermission(activity, requiredPermission) == PermissionChecker.PERMISSION_DENIED) {
//            requestPermissions(activity, arrayOf(requiredPermission), 101)
//        }
//    }



    fun startRecognition(){
        sr?.startListening(recognizerIntent)
    }

    fun stopRecognition(){
        sr?.stopListening()
    }

    fun onDestroy(){
        sr?.destroy()
    }



}