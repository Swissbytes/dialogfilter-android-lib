package ch.swissbytes.dialogfilter

import android.content.Context
import android.text.InputType
import android.widget.TextView
import ch.swissbytes.syscomappbase.extensions.toFormattedString
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.utils.NumberConstraintTextWatcher
import ch.swissbytes.syscomappbase.views.NumberEditText
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import java.math.BigDecimal

class DialogInputQty(
    val context: Context,
    val content: String?,
    min: BigDecimal? = null,
    max: BigDecimal? = null,
    private val title: String? = null,
    allowDecimal: Boolean = false,
    listener: GenericListener<BigDecimal>
) {

    var input: NumberEditText? = null
    init {
        val dialog = MaterialDialog(context).show {
            customView(R.layout.number_dialog)
            title?.let { title(text = it) }
            positiveButton(res = R.string.confirm) {
                input?.text.toString().toBigDecimalOrNull()?.apply {
                    listener.invoke(this)
                }
            }
        }
        val v = dialog.getCustomView()
        input = v.findViewById(R.id.input)

        input?.inputType = if (allowDecimal)
            InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        else
            InputType.TYPE_CLASS_NUMBER

        input?.requestFocus()
        content?.let{
            val label = v.findViewById<TextView>(R.id.label)
            label?.text = it
        }
        val subLabel = v.findViewById<TextView>(R.id.sub_label)
        subLabel?.text = context.getString(R.string.max_qty_format, max.toFormattedString())
        input?.addTextChangedListener(NumberConstraintTextWatcher(min, max))
    }

}