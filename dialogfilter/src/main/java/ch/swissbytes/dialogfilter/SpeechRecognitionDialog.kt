package ch.swissbytes.dialogfilter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import androidx.fragment.app.FragmentActivity
import ch.swissbytes.dialogfilter.extensions.requestRecordAudioPermissionIfNeeded
import ch.swissbytes.syscomappbase.extensions.showToast
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.afollestad.materialdialogs.customview.customView

class SpeechRecognitionDialog(
    val activity: FragmentActivity,
    private val onRequestForManualDigit: SimpleListener,
    val onResult: GenericListener<String>
) {

    private val TAG = this::class.java.simpleName
    private var sr: SpeechRecognizer? = null
    var dialog: MaterialDialog? = null

    val recognizerIntent by lazy {
        Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "voice.recognition.test")
            putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-ES");
            putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
        }
    }

    init {
        activity.requestRecordAudioPermissionIfNeeded()
    }



    private var recognitionListener = object: RecognitionListener {
        override fun onReadyForSpeech(params: Bundle) {
            Log.d(TAG, "onReadyForSpeech")
            dialog?.setActionButtonEnabled(WhichButton.POSITIVE, true)
        }

        override fun onBeginningOfSpeech() {
            Log.d(TAG, "onBeginningOfSpeech")
        }

        override fun onRmsChanged(rmsdB: Float) {
            Log.d(TAG, "onRmsChanged")
        }

        override fun onBufferReceived(buffer: ByteArray) {
            Log.d(TAG, "onBufferReceived")
        }

        override fun onEndOfSpeech() {
            Log.d(TAG, "onEndofSpeech")
            dialog?.setActionButtonEnabled(WhichButton.POSITIVE, false)
        }

        override fun onError(error: Int) {
            Log.d(TAG, "error $error")
            with(activity){
                when (error){
//                    SpeechRecognizer.ERROR_AUDIO -> { showToast(R.string. voice_recognition_error_audio) }
                    SpeechRecognizer.ERROR_NO_MATCH -> {
                        if (dialog != null){
                            sr?.startListening(recognizerIntent)
                            showToast(R.string. voice_recognition_error_not_match)
                        }
                    }
                    SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> { showToast(R.string. voice_recognition_error_busy) }
                    SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS -> { showToast(R.string. voice_recognition_error_permission) }
                }
            }
        }

        override fun onResults(results: Bundle) {
            Log.d(TAG, "onResults $results")
            val data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            if (data != null && data.size > 0){
                val result = data[0].replace(" ", "").trim()
                onResult.invoke(result)
                dialog?.dismiss()
            }
        }

        override fun onPartialResults(partialResults: Bundle) {
            Log.d(TAG, "onPartialResults")
        }

        override fun onEvent(eventType: Int, params: Bundle) {
            Log.d(TAG, "onEvent $eventType")
        }
    }

    init {
        sr = SpeechRecognizer.createSpeechRecognizer(activity)
        sr!!.setRecognitionListener(recognitionListener)
    }

    fun startRecognition(){
        dialog = MaterialDialog(activity as Context).show {
            customView(R.layout.voice_recognition)
            negativeButton(R.string.typing) {
                sr?.stopListening()
                onRequestForManualDigit.invoke()
            }
            positiveButton(R.string.confirm) {
                sr?.stopListening()
            }
            setActionButtonEnabled(WhichButton.POSITIVE, false)
            onDismiss {
                dialog = null
                sr?.destroy()
            }
        }

        sr?.startListening(recognizerIntent)
    }
}