package ch.swissbytes.dialogfilter

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView

class DialogLoader {

    var dialog: MaterialDialog? = null

    fun show(context: Context, title: String? = null){
        dialog = MaterialDialog(context).show {
            customView(R.layout.loader_dialog)
            title?.let { title(text = it) }
        }
    }

    fun hide(){
        dialog?.dismiss()
    }
}