package ch.swissbytes.dialogfilter

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.list.listItemsSingleChoice

class DialogFilter(private val windowContext: Context) {

    private val TAG = this::class.java.simpleName

    fun show(
        items: List<String>,
        @StringRes titleRes: Int? = null,
        @StringRes hintRes: Int = R.string.search_hint,
        minItemsCountForShowingSearch: Int = 4,
        listener: (Pair<Int, String>) -> Unit
    ){
        if (items.size >= minItemsCountForShowingSearch){
            showFilterDialog(items, titleRes, hintRes, listener)
        } else {
            MaterialDialog(windowContext).show {
                listItemsSingleChoice(items = items) { _, index, text ->
                    listener.invoke(Pair(index, text.toString()))
                }
            }
        }
    }

    private fun showFilterDialog(
        items: List<String>,
        @StringRes titleRes: Int? = null,
        @StringRes hintRes: Int = R.string.search_hint,
        listener: (Pair<Int, String>) -> Unit
    ){
        val dialog = MaterialDialog(windowContext).show {
            customView(R.layout.filter_dialog, noVerticalPadding = true)
            titleRes?.let { title(it) }
        }

        val viewDialog = dialog.getCustomView()
        val rv = viewDialog.findViewById<RecyclerView>(R.id.rv)
        with(rv){
            adapter = Adapter(items.toMutableList()) {
                listener.invoke(it)
                dialog.dismiss()
            }
        }

        val icon = viewDialog.findViewById<ImageView>(R.id.icon)
        val search = viewDialog.findViewById<EditText>(R.id.search_tv).apply { setHint(hintRes) }
        icon.setOnClickListener { with (search){ if (isFocused) clearFocus() else requestFocus() } }
        search.setOnFocusChangeListener { v, hasFocus ->
            icon.setImageResource(if (hasFocus) R.drawable.ic_close_black_24dp else R.drawable.ic_search_black_24dp)
            if (!hasFocus){
                search.setText("")
                windowContext.hideKeyboard(v)
            }
        }
        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                s?.let { text ->
                    (rv.adapter as Adapter).updateList(items.filter { it.contains(text.toString()) })
                }
            }
        })
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    class Adapter(val items: MutableList<String>, val listener: (Pair<Int, String>) -> Unit): RecyclerView.Adapter<Adapter.ViewHolder>() {

        override fun getItemCount(): Int = items.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.md_listitem_singlechoice,
                    parent,
                    false
                )
            )

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            holder.tv.text = item
            holder.itemView.setOnClickListener {
                listener.invoke(Pair(position, item))
            }
        }

        fun updateList(newItems: List<String>){
            items.clear()
            items.addAll(newItems)
            notifyDataSetChanged()
        }

        class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
            val tv = view.findViewById<TextView>(R.id.md_title)
        }

    }

}

