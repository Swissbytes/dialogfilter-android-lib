package ch.swissbytes.dialogfilter.extensions

import android.content.Context
import android.text.TextWatcher
import android.util.AttributeSet
import android.widget.EditText


class ExtendedEditText : EditText {
    private var mListeners: MutableList<TextWatcher> = mutableListOf()

    constructor(ctx: Context) : super(ctx) {}

    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs) {}

    constructor(ctx: Context, attrs: AttributeSet, defStyle: Int) : super(ctx, attrs, defStyle) {}

    override fun addTextChangedListener(watcher: TextWatcher) {
        mListeners.add(watcher)
        super.addTextChangedListener(watcher)
    }

    override fun removeTextChangedListener(watcher: TextWatcher) {
        if (mListeners.isNotEmpty()) {
            mListeners.indexOf(watcher).takeIf { it > 0 }?.let { mListeners.removeAt(it) }
        }
        super.removeTextChangedListener(watcher)
    }

    fun clearTextChangedListeners() {
        if (mListeners.isNotEmpty()) {
            mListeners.forEach {
                super.removeTextChangedListener(it)
            }
            mListeners.clear()
        }
    }
}