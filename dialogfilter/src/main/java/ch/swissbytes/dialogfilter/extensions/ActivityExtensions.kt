package ch.swissbytes.dialogfilter.extensions

import android.Manifest
import android.app.Activity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import ch.swissbytes.syscomappbase.listeners.GenericListener
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice

fun Activity.SpeechRecognitionDialog(list: List<String>, listener: GenericListener<Int>){
    MaterialDialog(this).show {
        listItemsSingleChoice(items = list) { _, index, _ ->
            listener.invoke(index)
        }
    }
}

fun Activity.requestRecordAudioPermissionIfNeeded() {
    val requiredPermission = Manifest.permission.RECORD_AUDIO

    // If the user previously denied this permission then showItemsCount a message explaining why
    // this permission is needed
    if (PermissionChecker.checkCallingOrSelfPermission(this, requiredPermission) == PermissionChecker.PERMISSION_DENIED) {
        ActivityCompat.requestPermissions(this, arrayOf(requiredPermission), 101)
    }
}

fun Activity.dialogListItemsSingleChoice(list: List<String>, listener: GenericListener<Int>): MaterialDialog{
    return MaterialDialog(this).show {
        listItemsSingleChoice(items = list) { _, index, _ ->
            listener.invoke(index)
        }
    }
}