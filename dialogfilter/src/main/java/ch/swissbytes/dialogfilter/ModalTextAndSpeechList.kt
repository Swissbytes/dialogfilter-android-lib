package ch.swissbytes.dialogfilter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.dialogfilter.extensions.ExtendedEditText
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.utils.SimpleTextWatcher
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.customListAdapter


class ModalTextAndSpeechList(
    private val activity: FragmentActivity
){

    private val TAG = this::class.java.simpleName

    fun showItemsCount(count: Int, @StringRes hintRes: Int = R.string.edit_serial, onVoiceRecognitionError: GenericListener<Int>? = null, onPositive: GenericListener<MutableList<String>>){
        val list = (1..count).map { "" }.toMutableList()
        MaterialDialog(activity as Context).show {
            title(R.string.speech_recognition_voice_dialog_title)
            customListAdapter(ControlledSerialsAdapter(list, hintRes, onVoiceRecognitionError))
            positiveButton(R.string.confirm) {
                onPositive.invoke(list)
            }
        }
    }

}

class ControlledSerialsAdapter(
    val list: MutableList<String>,
    @StringRes val hintRes: Int,
    private val onVoiceRecognitionError: GenericListener<Int>? = null
): RecyclerView.Adapter<ControlledSerialsAdapter.EditTextViewViewHolder>() {

    private val TAG = this::class.java.simpleName
    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditTextViewViewHolder =
        EditTextViewViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.edittext_speech_row, parent, false))

    override fun onBindViewHolder(holder: EditTextViewViewHolder, position: Int) {
        with (holder){
            with(editText){
                clearTextChangedListeners()
                setText(list[holder.adapterPosition])
                setHint(hintRes)
                addTextChangedListener(SimpleTextWatcher {
                    list[adapterPosition] = it
                })
            }

            with(voiceRecognitionView){
                onResult = {
                    list[adapterPosition] = it
                    editText.setText(list[adapterPosition])
                }
                onError = onVoiceRecognitionError
            }
        }

    }

    override fun onViewRecycled(holder: EditTextViewViewHolder) {
        list[holder.adapterPosition] = holder.editText.text.toString()
    }

    class EditTextViewViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val editText by lazy { view.findViewById<ExtendedEditText>(R.id.edittext) }
        val voiceRecognitionView by lazy { view.findViewById<SpeechRecognitionButton>(R.id.voice) }
    }
}