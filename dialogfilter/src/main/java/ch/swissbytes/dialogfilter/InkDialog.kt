package ch.swissbytes.dialogfilter

import android.graphics.Bitmap
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.github.gcacace.signaturepad.views.SignaturePad

class InkDialog(private val context: AppCompatActivity) {

    private val TAG = this.javaClass.simpleName

    private var padView: SignaturePad? = null
    internal var dialog: MaterialDialog? = null
    private var ink: Boolean = false
    private var isSign = false


    fun showDialog(listener: (Bitmap?) -> Unit) {
        if (dialog != null) { return }

        dialog = MaterialDialog(context).show {
            customView(R.layout.signature_dialog)
            title(R.string.sign)
            positiveButton(R.string.confirm)
            negativeButton(R.string.close)
            positiveButton {
                val bitmap = padView?.signatureBitmap
                listener.invoke(if (isSign) bitmap else null)
                setOnDismissListener { dialog = null }
            }
        }

        dialog?.getCustomView()?.apply {
            padView = findViewById(R.id.signature_pad)
            val btnClear = findViewById<Button>(R.id.clear)
            findViewById<CheckBox>(R.id.cb)?.setOnCheckedChangeListener { _, checked ->
                isSign = !checked
                padView?.visibility = if (checked) View.INVISIBLE else View.VISIBLE
                if (checked) btnClear?.visibility = View.INVISIBLE
            }
            btnClear?.setOnClickListener { padView?.clear() }
            padView?.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {}

                override fun onSigned() {
                    if (!ink) {
                        ink = true
                        isSign = true
                        btnClear?.visibility = View.VISIBLE
                    }
                }

                override fun onClear() {
                    if (ink) {
                        ink = false
                        isSign = false
                        btnClear?.visibility = View.INVISIBLE
                    }
                }
            })
        }

    }
}