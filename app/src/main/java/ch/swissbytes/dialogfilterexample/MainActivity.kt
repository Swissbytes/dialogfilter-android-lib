package ch.swissbytes.dialogfilterexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ch.swissbytes.dialogfilter.DialogInputQty
import ch.swissbytes.dialogfilter.ModalTextAndSpeechList
import ch.swissbytes.dialogfilter.extensions.requestRecordAudioPermissionIfNeeded
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal

class MainActivity : AppCompatActivity() {

    private val TAG = this::class.java.simpleName
    private val modal by lazy { ModalTextAndSpeechList(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn.setOnClickListener {

//            modal.showItemsCount(10,
//                onVoiceRecognitionError = {
//                    Log.e(TAG, "it: $it")
//                }){
//
//                }

            /**
                 * This will call [onPaymentDetailChange]
                 */
            DialogInputQty(
                context = this,
                content = "Hello",
                min = BigDecimal.ONE,
                max = BigDecimal.TEN,
                allowDecimal = true
            ){

                /**
                 * This will call [onPaymentDetailChange]
                 */
            }

        }

        requestRecordAudioPermissionIfNeeded()



//        with(voice){
//            onResult = {
//                Log.i(TAG, "onResut $it")
//            }
//
//            onError = {
//                Log.e(TAG, "onError: ${getString(it)}")
//            }
//
//            onSpeechStart = {
//                Log.i(TAG, "onSpeechStart")
//            }
//
//            onSpeechStop = {
//                Log.i(TAG, "onSpeechStop")
//            }
//        }
    }
}
